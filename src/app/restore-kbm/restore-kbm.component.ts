import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AngularFirestore, AngularFirestoreCollection} from "angularfire2/firestore";

export class Client {
  surname: string;
  name: string;
  secondName: string;
  birthDate: string;
  phoneNumber: string;
  email: string;
  drivingSerial: string;
  drivingNumber: string;
  passportSerial: string;
  passportNumber: string;
}

@Component({
  selector: 'app-restore-kbm',
  templateUrl: './restore-kbm.component.html',
  styleUrls: ['./restore-kbm.component.scss']
})
export class RestoreKbmComponent implements OnInit {
  restoreKbm: FormGroup;
  masks = {
    birthDate: [/[0-3]/, /\d/, '.', /[0-1]/, /\d/, '.', /[1-2]/, /\d/, /\d/, /\d/],
    phoneNumber: [8, '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
    drivingSerial: [/\d/, /\d/, /\d/, /\d/],
    drivingNumber: [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/],
    passportSerial: [/\d/, /\d/, /\d/, /\d/],
    passportNumber: [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/]
  };
  private bundlesRef: AngularFirestoreCollection;

  constructor(private db: AngularFirestore) {
    this.bundlesRef = this.db.collection<Client>('clients');
  }

  ngOnInit() {
    this.restoreKbm = new FormGroup({
      surname: new FormControl('', [Validators.required]),
      name: new FormControl('', [Validators.required]),
      secondName: new FormControl('', [Validators.required]),
      birthDate: new FormControl('', [Validators.required]),
      phoneNumber: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      drivingSerial: new FormControl('', [Validators.required]),
      drivingNumber: new FormControl('', [Validators.required]),
      passportSerial: new FormControl('', [Validators.required]),
      passportNumber: new FormControl('', [Validators.required]),
    });
  }

  onSave() {
    this.bundlesRef.add(this.restoreKbm.value).then(() => alert('done'));
  }
}
