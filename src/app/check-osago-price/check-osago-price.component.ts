import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-check-osago-price',
  templateUrl: './check-osago-price.component.html',
  styleUrls: ['./check-osago-price.component.scss']
})
export class CheckOsagoPriceComponent implements OnInit {
  drivingExperience: number;
  driverClass: number;
  overpay: number;
  drivingExperienceOptions;
  driverClassesOptions;
  curStep = 'initial';

  constructor() { }

  ngOnInit() {
    this.drivingExperienceOptions = [
      {value: 1, text: "1"},
      {value: 2, text: "2"},
      {value: 3, text: "3"},
      {value: 4, text: "4"},
      {value: 5, text: "5"},
      {value: 6, text: "6"},
      {value: 7, text: "7"},
      {value: 8, text: "8"},
      {value: 9, text: "9"},
      {value: 10, text: "10+"}
    ];

    this.driverClassesOptions = [
      {value: 0, text: "М", tooltip: "КБМ = 2.45"},
      {value: 0, text: "0", tooltip: "КБМ = 2.3"},
      {value: 1, text: "1", tooltip: "КБМ = 1.55"},
      {value: 2, text: "2", tooltip: "КБМ = 1.4"},
      {value: 3, text: "3", tooltip: "КБМ = 1(не найден)"},
      {value: 4, text: "4", tooltip: "КБМ = 0.95"},
      {value: 5, text: "5", tooltip: "КБМ = 0.9"},
      {value: 6, text: "6", tooltip: "КБМ = 0.85"},
      {value: 7, text: "7", tooltip: "КБМ = 0.8"},
      {value: 8, text: "8", tooltip: "КБМ = 0.75"},
      {value: 9, text: "9", tooltip: "КБМ = 0.7"},
      {value: 10, text: "10", tooltip: "КБМ = 0.65"},
      {value: 11, text: "11", tooltip: "КБМ = 0.6"},
      {value: 12, text: "12", tooltip: "КБМ = 0.55"},
      {value: 13, text: "13", tooltip: "КБМ = 0.5"}
    ];
  }

  onDrivingExperienceSelect(selectedExperience: number) {
    this.drivingExperience = selectedExperience + 3;
  }

  onDriverClassSelect(dClass: number) {
    this.driverClass = dClass;
    if(this.drivingExperience > this.driverClass) {
      let stag = 1 - (this.drivingExperience - 3) * 0.05;
      let bonus = 1 - (this.driverClass - 3) * 0.05;
      this.overpay = Math.floor((1 - (stag/bonus)) * 100);
      this.curStep = 'warning';
    } else if(this.drivingExperience === this.driverClass) {
      this.curStep = 'success';
    } else {
      this.curStep = 'buyOsago'
    }
  }
}
