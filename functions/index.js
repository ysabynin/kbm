const functions = require('firebase-functions');
// var mailgun = require('mailgun-js')({apiKey, domain});
const nodemailer = require('nodemailer');

exports.sendWelcomeEmail = functions.database.ref('clients/{uid}').onWrite(event => {
  // only trigger for new users [event.data.previous.exists()]
  // do not trigger on delete [!event.data.exists()]
  if (!event.data.exists() || event.data.previous.exists()) {
    return
  }

  let user = event.data.val();
  let {email} = user;

  let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: 'diagnostic.to.online@gmail.com',
      pass: '12345678max'
    }
  });

  // setup email data with unicode symbols
  let mailOptions = {
    from: 'diagnostic.to.online@gmail.com',
    to: 'evgensabyka@mail.ru', // list of receivers
    subject: 'Новая заявка на оформление КБМ', // Subject line
    text: '' + event.data.val() // plain text body
    // html: '<b>Hello world?</b>' // html body
  };

  // send mail with defined transport object
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log('Message sent: %s', info.messageId);
    // Preview only available when sending through an Ethereal account
    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
  }).catch(function () {
    
  })
});
